package labactivity7;
public interface ISandwich {
    String getFilling();
    String addFilling();
    boolean isVegeterian();
}
